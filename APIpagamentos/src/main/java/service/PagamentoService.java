package service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import controller.PagamentoController;
import model.Pagamento;

@Path("/PagamentoService")
public class PagamentoService {
	
	public static PagamentoController pagamentoController = new PagamentoController();  

    @POST
    @Path("/Pagamento")
    @Produces(MediaType.APPLICATION_JSON)
    public Pagamento solicitaPagamento(Pagamento pagamento) {
    	   	    	
    	return pagamentoController.solicitarPagamento(pagamento);             
               
    }
    
    @GET
    @Path("/Pagamento")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pagamento> obterPagamento(@QueryParam("id") String idPagamento) {
    	
    	if(idPagamento != null) {
    		
    		List<Pagamento> pagamento = new ArrayList<Pagamento>();
    		pagamento.add(pagamentoController.obterPagamento(idPagamento,false));
    		
    		return pagamento;
    		
    	} else {
    		    		
    		return pagamentoController.obterPagamento(false);
    		
    	}          
               
    }
    
    @DELETE
    @Path("/Pagamento")
    @Produces(MediaType.APPLICATION_JSON)
    public Pagamento estornaPagamento(@QueryParam("id") String idPagamento) {
    	
		return pagamentoController.estornarPagamento(idPagamento);            
               
    }
    
    @GET
    @Path("/Estorno")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pagamento> obterEstorno(@QueryParam("id") String idEstorno) {
    	
    	if(idEstorno != null) {
    		
    		List<Pagamento> pagamento = new ArrayList<Pagamento>();
    		pagamento.add(pagamentoController.obterPagamento(idEstorno,true));
    		
    		return pagamento;
    		
    	} else {
    		    		
    		return pagamentoController.obterPagamento(true);
    		
    	}          
               
    }
    
}
