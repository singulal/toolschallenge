package model;

public class FormaPagamento {
	public String tipo;
	public int parcelas;
	
	public FormaPagamento() {}
	
	public FormaPagamento(String tipo, int parcelas) 
	{
		this.setTipo(tipo);
		this.setParcelas(parcelas);
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
	
		this.tipo = tipo; 
		
		
	}

	public String getParcelas() {
		return String.valueOf(parcelas);
	}

	public void setParcelas(int parcelas) {
		this.parcelas = parcelas;
	}
}
