package model;

public class Descricao {

	private String valor;
	private String dataHora;
	private String estabelecimento;
	private Long nsu;
	private Long codigoautoriza;
	private String status;
	
	public Descricao(String valor, String dataHora, String estabelecimento,
			Long nsu, Long codigoautoriza, String status) 
	{
		
		this.setValor(valor);
		this.setDataHora(dataHora);
		this.setEstabelecimento(estabelecimento);
		this.setNsu(nsu);
		this.setCodigoautoriza(codigoautoriza);
		this.setStatus(status);
		
	}
	
	public Descricao() {}
	
	public Descricao(String valor, String dataHora, String estabelecimento) 
	{		
		this.setValor(valor);
		this.setDataHora(dataHora);
		this.setEstabelecimento(estabelecimento);
		
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String dataHora) {
		
		this.dataHora = dataHora;
		
	}

	public String getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(String estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public String getNsu() {
		return String.valueOf(nsu);
	}

	public void setNsu(long nsu) {
		this.nsu = nsu;
	}

	public String getCodigoautoriza() {
		return String.valueOf(codigoautoriza);
	}

	public void setCodigoautoriza(long codigoautoriza) {
		this.codigoautoriza = codigoautoriza;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
