package model;

public class Transacao {
	
	private Long cartao;
	private Long id;
	private Descricao descricao; 
	private FormaPagamento formaPagamento;
	
	public Transacao(){}
	
	public Transacao(Long cartao, Long id, Descricao descricao, FormaPagamento formaPagamento) {
		
		this.setCartao(cartao);
		this.setId(id);
		this.setDescricao(descricao);
		this.setFormaPagamento(formaPagamento);
		
	}

	public String getCartao() {
		return cartao.toString();
	}

	public void setCartao(Long cartao) {
		this.cartao = cartao;
	}

	public String getId() {
		return id.toString();
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Descricao getDescricao() {
		return descricao;
	}

	public void setDescricao(Descricao descricao) {
		this.descricao = descricao;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
}
