package controller;

import java.util.List;

import dao.PagamentoDAO;
import model.Pagamento;

public class PagamentoController {
	
	private PagamentoDAO pagamentoDAO = new PagamentoDAO();
	
	public List<Pagamento> obterPagamento(boolean estorno) {
		
		return pagamentoDAO.obterPagamento(estorno);
				
	}
	
	public Pagamento obterPagamento(String idPagamento,boolean estorno) {
				
		return pagamentoDAO.obterPagamento(idPagamento,estorno);
				
	}
	
	public Pagamento solicitarPagamento(Pagamento pagamento) {
		
		return pagamentoDAO.registraPagamento(pagamento);
		
	}
	
	public Pagamento estornarPagamento(String idPagamento) {
		
		Pagamento pagamento = obterPagamento(idPagamento,false);
		
		if(pagamento!= null) {
			
			pagamento.getTransacao().getDescricao().setStatus("CANCELADO");
			pagamentoDAO.estornaPagamento(pagamento);
			
		}
				
		return pagamento;
				
	}
	
}
