package dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Pagamento;

public class PagamentoDAO {
	
	Long idPagamentoAutoIncrement = new Long(0);
	List<Pagamento> listaPagamento = new ArrayList<Pagamento>();
	List<Pagamento> listaEstorno = new ArrayList<Pagamento>();
	
	public Pagamento registraPagamento(Pagamento pagamento) {
			
			if(validaPagamento(pagamento)){
				
				idPagamentoAutoIncrement++;
				pagamento.getTransacao().setId(idPagamentoAutoIncrement);
				pagamento.getTransacao().getDescricao().setStatus("AUTORIZADO");
				pagamento.getTransacao().getDescricao().setNsu(4400000000L + idPagamentoAutoIncrement);
				pagamento.getTransacao().getDescricao().setCodigoautoriza(5500000000L + idPagamentoAutoIncrement);
				listaPagamento.add(pagamento);
			
			} else {
				
				pagamento.getTransacao().getDescricao().setStatus("NEGADO");
			}
				
		return pagamento;
		
	}
	
	private boolean validaPagamento(Pagamento pagamento) {
		
		String tipo = pagamento.getTransacao().getFormaPagamento().getTipo();
		String dataHora = pagamento.getTransacao().getDescricao().getDataHora();
		int parcelas = Integer.parseInt(pagamento.getTransacao().getFormaPagamento().getParcelas());
		
		//verifica se o tipo de pagamento é válido
		if(!(tipo.equals("AVISTA") || tipo.equals("PARCELADO LOJA") || tipo.equals("PARCELADO EMISSOR"))) {
			return false;
		}
		
		if(tipo.equals("AVISTA") && (parcelas > 1 || parcelas == 0)) {
			return false;
		}
		
		if((tipo.equals("PARCELADO LOJA") || tipo.equals("PARCELADO EMISSOR")) && parcelas <= 1 )  
		{
			return false;
		}
		
		//verifica se o formato de data é válido
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
		
		try {
			
			Date dataFormatada = formatoData.parse(dataHora);	
			
		}catch (java.text.ParseException e) {
			
			return false;
		
		}
				
		return true;
		
	}

	public Pagamento estornaPagamento(Pagamento pagamento) {
		
		
		listaEstorno.add(pagamento);
		//precisa buscar o pagamento novamente porque a lista não simula bem um banco de dados
		listaPagamento.remove(obterPagamento(pagamento.getTransacao().getId(),false));
		
		return pagamento;
		
	}
	//estorno é um pagamento cancelado, não justifica criar um controller e um DAO específico
	public Pagamento obterPagamento(String idPagamento,boolean estorno) {
		
		List<Pagamento> listaPagamentoEstorno = (estorno ) ?  listaEstorno : listaPagamento ;
		
		for(Pagamento pagamento: listaPagamentoEstorno) {
			
			if(pagamento.getTransacao().getId().equals(idPagamento)) {
				
				return pagamento;
				
			}
			
		}
		
		return null;
		
	}
	
	public List<Pagamento> obterPagamento(boolean estorno) {
		
		List<Pagamento> listaPagamentoEstorno = (estorno ) ?  listaEstorno : listaPagamento;
		
		return listaPagamentoEstorno;
		
		
	}
}
