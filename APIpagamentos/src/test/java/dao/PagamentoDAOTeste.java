package dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Descricao;
import model.FormaPagamento;
import model.Pagamento;
import model.Transacao;


public class PagamentoDAOTeste {
	private PagamentoDAO pagamentoDAO = new PagamentoDAO();;
	
	@Test
	public void solicitarPagamento() {
		
		Pagamento pagamentoTeste = pagamentoDAO.registraPagamento(preparaPagamentoSolicitacao(4444412345123L, 1234512345123L, "200.20", "01/05/2021 18:30",
				"Mundo Cão", "445559866666", "725229866666", "", "AVISTA", 1));
		assertEquals("AUTORIZADO", pagamentoTeste.getTransacao().getDescricao().getStatus());

		
	}
	
	@Test
	public void solicitarPagamentoDataInvalida() {
		
		Pagamento pagamentoTeste = pagamentoDAO.registraPagamento(preparaPagamentoSolicitacao(4444412345123L, 1234512345123L, "200.20", "01-05-2021 18:30:00",
				"Mundo Cão", "445559866666", "725229866666", "", "AVISTA", 1));
		assertEquals("NEGADO", pagamentoTeste.getTransacao().getDescricao().getStatus());

		
	}

	@Test
	public void solicitarPagamentoDataInvalidaMySQL() {
		
		Pagamento pagamentoTeste = pagamentoDAO.registraPagamento(preparaPagamentoSolicitacao(4444412345123L, 1234512345123L, "200.20", "2021-01-05 23:59:00",
				"Mundo Cão", "445559866666", "725229866666", "", "AVISTA", 1));
		assertEquals("NEGADO", pagamentoTeste.getTransacao().getDescricao().getStatus());

		
	}
	
	@Test
	public void solicitarPagamentoTipoTransacaoInvalido() {
		
		Pagamento pagamentoTeste = pagamentoDAO.registraPagamento(preparaPagamentoSolicitacao(4444412345123L, 1234512345123L, "200.20", "01/05/2021 18:30:00",
				"Mundo Cão", "445559866666", "725229866666", "", "PARCELA", 1));
		assertEquals("NEGADO", pagamentoTeste.getTransacao().getDescricao().getStatus());
		
	}
	
	@Test
	public void solicitarPagamentoAvistaComParcela() {
		
		Pagamento pagamentoTeste = pagamentoDAO.registraPagamento(preparaPagamentoSolicitacao(4444412345123L, 1234512345123L, "200.20", "01/05/2021 18:30:00",
				"Mundo Cão", "445559866666", "725229866666", "", "AVISTA", 3));
		assertEquals("NEGADO", pagamentoTeste.getTransacao().getDescricao().getStatus());
		
	}
	
	@Test
	public void solicitarPagamentoParceladoComParcelaUnica() {
				
		Pagamento pagamentoTeste = pagamentoDAO.registraPagamento(preparaPagamentoSolicitacao(4444412345123L, 1234512345123L, "200.20", "01/05/2021 18:30:00",
				"Mundo Cão", "445559866666", "725229866666", "", "PARCELADO LOJA", 1));
		assertEquals("NEGADO", pagamentoTeste.getTransacao().getDescricao().getStatus());
	}
	
	public Pagamento preparaPagamentoSolicitacao(Long cartao, Long id, String valor, String dataHora,
			String estabelecimento, String nsu, String codigoautoriza, String status, String tipo, int parcelas) {
			
			FormaPagamento formaPagamento = new FormaPagamento (tipo, parcelas);
			Descricao descricao = new Descricao( valor,dataHora, estabelecimento);
			Transacao transacao = new Transacao(cartao, id, descricao, formaPagamento);
			Pagamento pagamento = new Pagamento(transacao);
		
			return pagamento;
	
		}
}
