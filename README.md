# API de Pagamentos
Essa é uma aplicação feita em Java, utilizando o Maven para automação e gerenciamento de dependências, cujo objetivo é fornecer uma API de pagamentos que implementa as operaçoes de inserção, consulta e estorno de pagamentos no padrão REST.
Todas as dependências se encontram no arquivo pom.xml

## Operações
***	
Pagamento:
* [POST] /APIpagamentos/PagamentoService/Pagamento
* [GET] /APIpagamentos/PagamentoService/Pagamento?id=""
* [GET] /APIpagamentos/PagamentoService/Pagamento/
* [DELETE] /APIpagamentos/PagamentoService/Pagamento?id=""
	
***
Estorno:
* [GET] /APIpagamentos/PagamentoService/Estorno?id=""
* [GET] /APIpagamentos/PagamentoService/Estorno/
	
	
## Pré-requisito
É necessário uma versão Java 1.7+

## Para executar localmente
Clone e execute o comando abaixo:

	mvn tomcat:run
	A API estará disponível em http://localhost:8080/

## Para gerar o arquivo war para o tomcat no Eclipse 
Clone e execute o comando abaixo:

	compile war:war
	

